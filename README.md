It is a simple app to show recipes.

![recipeShown.png](https://bitbucket.org/repo/Aj5bAd/images/965092733-recipeShown.png)

Searched recipe loads from your Core Data, otherwise from: http://www.recipepuppy.com/

![recipeMenu.png](https://bitbucket.org/repo/Aj5bAd/images/3821817588-recipeMenu.png)

Backspacing search text field app loads Default URL.

Can work offline.