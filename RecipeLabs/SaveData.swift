//
//  SaveData.swift
//  RecipeLabs
//
//  Created by Andrei Momot on 3/6/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit
import CoreData

class SaveData {
  enum EntityName: String {
    case recipeEntity = "RecipeEntity"
  }

  enum PredicateName: String {
    case title = "title == %@"
  }

  enum Key: String {
    case title
    case ingredients
    case reference
    case thumbnail
  }

  func getContext () -> NSManagedObjectContext {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      fatalError()
    }
    return appDelegate.persistentContainer.viewContext
  }

func storeRecipes(title: String, ingredients: String, reference: String, thumbnail: String,
                  completion: @escaping (RecipeEntity) -> Void) {
  let fetchRequest: NSFetchRequest<RecipeEntity> = RecipeEntity.fetchRequest()
  let predicate = NSPredicate(format: PredicateName.title.rawValue, title)
  fetchRequest.predicate = predicate

  do {
    let searchResults = try getContext().fetch(fetchRequest)
    if searchResults.count > 0 {
      return
    } else {

      let context = getContext()
      let entity =  NSEntityDescription.entity(forEntityName: EntityName.recipeEntity.rawValue, in: context)
      let object = NSManagedObject(entity: entity!, insertInto: context)

      object.setValue(title, forKey: Key.title.rawValue)
      object.setValue(ingredients, forKey: Key.ingredients.rawValue)
      object.setValue(reference, forKey: Key.reference.rawValue)
      if let imageURL = URL(string: thumbnail) {
        if let imageData = NSData(contentsOf: imageURL) {
          object.setValue(imageData, forKey: Key.thumbnail.rawValue)
        }
      }
      do {
        try context.save()
        guard let resultObject = object as? RecipeEntity else {
          fatalError()
        }
        completion(resultObject)
      } catch let error as NSError {
        print("Could not save \(error), \(error.userInfo)")
      } catch {
      }
    }
  } catch {
    print("Error with request: \(error)")
    }
  }
}
