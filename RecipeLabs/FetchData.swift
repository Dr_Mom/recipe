//
//  FetchData.swift
//  RecipeLabs
//
//  Created by Andrei Momot on 3/6/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit
import CoreData

class FetchData {
  enum PredicateName: String {
    case title = "title contains[c] %@"
  }
  func getContext () -> NSManagedObjectContext {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      fatalError()
    }
    return appDelegate.persistentContainer.viewContext
  }
  func fetchRecipes(searchName: String, completion: @escaping ([RecipeEntity]) -> Void) {
    let fetchRequest: NSFetchRequest<RecipeEntity> = RecipeEntity.fetchRequest()
    let predicate = NSPredicate(format: PredicateName.title.rawValue, searchName)
    fetchRequest.predicate = predicate
    do {
      let searchResults = try getContext().fetch(fetchRequest)
      completion(searchResults)
    } catch {
      print("Error with request: \(error)")
    }
  }
}
