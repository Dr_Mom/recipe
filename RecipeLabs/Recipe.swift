//
//  Recipes.swift
//  RecipeLabs
//
//  Created by Andrei Momot on 3/2/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import Foundation

struct Recipe {
  enum Key: String {
    case title = "title"
    case ingredients = "ingredients"
    case thumbnail = "thumbnail"
    case reference = "href"
  }
  var title: String?
  var ingredients: String?
  var image: String?
  var reference: String?

  init(dictionary: [String: Any]) {
    self.title = dictionary[Key.title.rawValue] as? String? ?? ""
    self.ingredients = dictionary[Key.ingredients.rawValue] as? String? ?? ""
    self.image = dictionary[Key.thumbnail.rawValue] as? String ?? ""
    self.reference = dictionary[Key.reference.rawValue] as? String ?? ""
  }
}
