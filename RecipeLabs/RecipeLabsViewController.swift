//
//  RecipeLabsViewController.swift
//  RecipeLabs
//
//  Created by Andrei Momot on 3/2/17.
//  Copyright © 2017 Dr_Mom. All rights reserved.
//

import UIKit

class RecipeLabsViewController: UIViewController {
  enum TableViewIdentifier: String {
    case recipeCell = "RecipeCell"
  }

  enum Key: String {
    case apiResults = "results"
  }

  enum StringValue: String {
    case backSpaceChar = "\\b"
    case defaultTitle = "Omelet"
  }

  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchTextField: UITextField!

  var fetchData = FetchData()
  var saveData = SaveData()
  var recipeEntity = [RecipeEntity]() {
    didSet {
      tableView.reloadData()
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    tableViewInitialSetUp()
  }

  private func tableViewInitialSetUp() {
    let nib = UINib(nibName: TableViewIdentifier.recipeCell.rawValue, bundle: nil)
    tableView.register(nib, forCellReuseIdentifier: TableViewIdentifier.recipeCell.rawValue)
    // Removes empty cells after footer.
    tableView.tableFooterView = UIView()
  }

  func loadDefaultRecipes() {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      fatalError()
    }
    let apiService = appDelegate.applicationManager.apiService

    apiService.defaultRecipe { (result) in
      switch result {
      case .success(let value):
        print(value)
        let response = value as? [String: Any]
        if let results = response?[Key.apiResults.rawValue] as? [[String: Any]] {
        for result in results {
          let recipeItem = Recipe(dictionary: result)
          self.saveData.storeRecipes(title: recipeItem.title!,
                                     ingredients: recipeItem.ingredients!,
                                     reference: recipeItem.reference!,
                                     thumbnail: recipeItem.image!) { object in
            self.recipeEntity.insert(object, at: 0)
          }
          }
        }
            case .failure(let error):
            print(error)
      }
    }
  }

  @IBAction func textFieldPrimaryActionTriggered(_ sender: Any) {

    loadSearchedRecipes()
    fetchData.fetchRecipes(searchName: searchTextField.text!) { recipes in
      self.recipeEntity = recipes
    }
  }

  func loadSearchedRecipes() {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      fatalError()
    }
    let apiService = appDelegate.applicationManager.apiService

    let recipe = searchTextField.text

    apiService.searchRecipeBy(name: recipe!) { (result) in
      switch result {
      case .success(let value):
        print(value)
        let response = value as? [String: Any]
        if let results = response?[Key.apiResults.rawValue] as? [[String: Any]] {
          for result in results {
            let recipeItem = Recipe(dictionary: result)
            self.saveData.storeRecipes(title: recipeItem.title!,
                                       ingredients: recipeItem.ingredients!,
                                       reference: recipeItem.reference!,
                                       thumbnail: recipeItem.image!) { object in
              self.recipeEntity.insert(object, at: 0)
            }
          }
        }
      case .failure(let error):
        print(error)
      }
    }
  }

  func goToLink(url: URL) {
      UIApplication.shared.open(url, options: [:], completionHandler: nil)
  }
}

// MARK: - UITableViewDataSource
extension RecipeLabsViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: TableViewIdentifier.recipeCell.rawValue)
      as? RecipeCell else {
      fatalError()
    }
    let recipeItem = recipeEntity[indexPath.row]
    cell.recipe = recipeItem
    return cell
  }
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return recipeEntity.count
  }
}

// MARK: - UITableViewDelegate
extension RecipeLabsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let selectedRecipe = recipeEntity[indexPath.row]
    if let url = URL(string: selectedRecipe.reference!) {
      self.goToLink(url: url)
      tableView.deselectRow(at: indexPath, animated: true)
    }
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }

  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    return 140.0
  }
}

// MARK: - UITextFieldDelegate
extension RecipeLabsViewController: UITextFieldDelegate {
  func textField(_ textField: UITextField,
                 shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool {

    let char = string.cString(using: String.Encoding.utf8)!
    let isBackSpace = strcmp(char, StringValue.backSpaceChar.rawValue)

    if isBackSpace == -92 {
      if var string = textField.text {
        if string.characters.count == 1 {
          loadDefaultRecipes()
          fetchData.fetchRecipes(searchName: StringValue.defaultTitle.rawValue, completion: { recipes in
            self.recipeEntity = recipes
          })
        }
      }
    }
    return true
  }
}
